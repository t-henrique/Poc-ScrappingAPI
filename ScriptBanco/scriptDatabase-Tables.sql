create database RobbysonScrapingApi

USE RobbysonScrapingApi
create table users(
	userid bigint identity primary key,
	name varchar(100),
	birthDate datetime,
	email varchar(50),
	password varchar(50),
	authMethod integer,
	cpf varchar(11),
	phone varchar(11)
)

