﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrapySharp.Network;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Network;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Html.Forms;

namespace POC_ScrapingApiService.ServicoScrap
{
    public class CustomScrap
    {
        public string DoScraping()
        {
            var scrapingBrowser = new ScrapingBrowser();
            scrapingBrowser.AllowAutoRedirect = true;
            scrapingBrowser.AllowMetaRedirect = true;
            
            //etapa de buscar atraves do CssSelect o titulo da pagina
            WebPage PageResult = scrapingBrowser.NavigateToPage(new Uri("http://www.uol.com.br/"));
            HtmlNode TitleNode = PageResult.Html.CssSelect(".font1").First();
            string PageTitle = TitleNode.InnerText;

            
            return PageTitle;
        }
    }
}