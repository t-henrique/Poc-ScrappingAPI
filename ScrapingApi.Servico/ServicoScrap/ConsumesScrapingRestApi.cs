﻿using Newtonsoft.Json;
using ScrapingApi.Dominio.Entities.Agent;
using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Dominio.Entities.PdfManagement;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;

namespace ScrapingApi.Servico
{
    /// <summary>
    /// Class destinated to communicates with the routes from Scrapings Tools. 
    /// </summary>
    public class ConsumesScrapingRestApi
    {
       
        private UrlApiBase clientUrl = new UrlApiBase();

        //public RetornoPdfSky returnPdf(LoginSky login)
        //{
        //    var sessionId = Guid.NewGuid();
        //    FaturaPdfSky agent = new FaturaPdfSky();
        //    var skyAgents = new SkyAgents();
        //    var loginJson = JsonConvert.SerializeObject(login);
        //    RetornoPdfSky retornoPdf = new RetornoPdfSky();
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            var urlOData = string.Format("{0}RunAgentReturnJson?agent={1}&logLevel=High&timeOut=120&sessionId={2}&pars={3}&logtofile=true", clientUrl.client.BaseAddress, skyAgents.AgentReturnFaturaPdf, sessionId, loginJson);

        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            var response = httpClient.GetAsync(urlOData).Result;
        //            response.EnsureSuccessStatusCode();
        //            var resultado = response.Content.ReadAsStringAsync().Result;

        //            agent = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<FaturaPdfSky>(resultado)).Result;

        //            if (String.IsNullOrEmpty(agent.data.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY))
        //            {
        //                throw new ArgumentNullException(resultado);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // teste de projeto
        //        retornoPdf.FaturaNum = "1234";
        //        retornoPdf.DescontoBonus = "desconto bonus";
        //        retornoPdf.Vencimento = "12/02/2014";
        //        retornoPdf.ValorTotal = "156,20";
        //        retornoPdf.ValorItem = "300,01";
        //        retornoPdf.Periodo = "15 dias";
        //        retornoPdf.Item = "item teste";
        //        retornoPdf.DataInicio = "01/03/2017";
        //        retornoPdf.DataFim = "01/04/2017";
        //        retornoPdf.CodigoDoClienteNum= "161825";

        //        return retornoPdf;
        //    }
           
            
        //    var pdfContent = ConvertPdfWithFileLayout(agent.data.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY, LayoutPath.pathLayout());
        //    return retornoPdf = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<RetornoPdfSky>(pdfContent)).Result;
        //}

        //public RetornoPdfSky returnPDFFileAlternativelly(string agentName)
        //{
        //    var sessionId = Guid.NewGuid();
        //    var pdfReturn = String.Empty;
        //    var retornoErro = String.Empty;

        //    RetornoPdfSky pdfSky = null;
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            if (startAgent(agentName, sessionId))
        //            {
        //                getAgentProgress(agentName, sessionId, retornoErro);

        //                Task task = delayGetAgentData(1000);
        //                pdfReturn = getAgentData(agentName, sessionId, retornoErro);
        //            }
        //            if (!String.IsNullOrEmpty(retornoErro))
        //            {
        //                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        //                return pdfSky = (RetornoPdfSky)jsonSerializer.DeserializeObject(pdfReturn);
        //                }
        //            else{
        //                //verificar este retorno e corrigir
        //                throw new NullReferenceException(retornoErro += new HttpResponseMessage(HttpStatusCode.NoContent).ToString());
        //        }}
        //    }
        //    catch (Exception ex)
        //    {
        //        //verificar este retorno e corrigir
        //        throw new NullReferenceException(retornoErro += new HttpResponseMessage(HttpStatusCode.InternalServerError).ToString());
        //    }
        //}

        //public RetornoPdfSky getPdfJsonTest()
        //{
        //    var pdfSky = new RetornoPdfSky();

        //    var caminho = @"C:\Users\Public\Documents\Content Grabber 2\Agents\SkyAgent_Fatura_UL\Data\Files\5a9580e9-3dcb-4044-bbed-673782edfd77\1b4e9ee9da2f42a7b1feeeb1d970231e\400249447410.html";
        //    var pdfContent = exec(caminho, LayoutPath.pathLayout());

        //    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

        //    pdfSky = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<RetornoPdfSky>(pdfContent)).Result;
        //     return pdfSky;
        //}

        //public ReturnUserDataSky returnUserDataFromClient(LoginSky login)
        //{
        //    ReturnUserDataSky userDataSky = null;
        //    var sessionId = Guid.NewGuid();
        //    var retornoErro = string.Empty;
        //    var loginJson = JsonConvert.SerializeObject(login);


        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            var url = string.Format("{0}RunAgentReturnJson?agent={1}&sessionId={2}&logLevel=High&logToFile=true&timeOut=80&pars={3}", clientUrl.client.BaseAddress, skyAgents.AgentReturnUserData, sessionId, loginJson);
        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    

        //            var response = httpClient.GetAsync(url).Result;
        //            response.EnsureSuccessStatusCode();

        //            var resultado = response.Content.ReadAsStringAsync().Result;

        //            userDataSky = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ReturnUserDataSky>(resultado)).Result;

        //            return userDataSky;

        //        }
        //    }
        //    catch(Exception ex){
        //        throw new ArgumentNullException("error 1");
        //    }
        //}


        //public string startsAgentWithSessionGetFaturaPdf(string agent, Guid sessionId)
        //{
        //    FaturaPdfSky fatura = null;

        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            /// passar guid na sessao do usuario
        //            ///

        //            //http://localhost:8004/ContentGrabber/RunAgentReturnJson?agent=sky
        //            var urlOData = string.Format("{0}RunAgentReturnJson?agent={1}&logLevel=High&timeOut=70", clientUrl.client.BaseAddress, agent);
        //            //var urlOData = string.Format("{0}GetAgentDataAsJson?agent={1}&logLevel=High&timeOut=70", client.BaseAddress, agentName);

        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            var response = httpClient.GetAsync(urlOData).Result;
        //            response.EnsureSuccessStatusCode();

        //            var resultado = response.Content.ReadAsStringAsync().Result;

        //            fatura = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<FaturaPdfSky>(resultado)).Result;

        //            if (String.IsNullOrEmpty(fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY.ToString()))
        //            {
        //                throw new ArgumentNullException(resultado);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    var str = fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY;

        //    var pdfContent = exec(str, LayoutPath.pathLayout());
        //    return pdfContent;
        //}

        //private bool startAgent(string agentName, Guid sessionId)
        //{
        //    var startStopAgentResult = new StartStopAgentResult();
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            var urlOData = string.Format("{0}StartAgent?agent={1}&sessionId={2}&timeOut=100", clientUrl.client.BaseAddress, agentName, sessionId);

        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            var response = httpClient.GetAsync(urlOData).Result;
        //            response.EnsureSuccessStatusCode();
        //            var resultado = response.Content.ReadAsStringAsync().Result;

        //            startStopAgentResult = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<StartStopAgentResult>(resultado)).Result;

        //            if (!startStopAgentResult.success)
        //            {
        //                throw new HttpCompileException(startStopAgentResult.message);
        //            }
        //            return startStopAgentResult.success;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.ToString());
        //    }
        //}

        //private void stopAgent(string agentName, Guid sessionId)
        //{
        //    var startStopAgentResult = new StartStopAgentResult();
        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            var urlOData = string.Format("{0}StopAgent?agent={1}&sessionId={2}", clientUrl.client.BaseAddress, agentName, sessionId);

        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //            var response = httpClient.GetAsync(urlOData).Result;
        //            response.EnsureSuccessStatusCode();
        //            var resultado = response.Content.ReadAsStringAsync().Result;

        //            startStopAgentResult = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<StartStopAgentResult>(resultado)).Result;

        //            if (!startStopAgentResult.success)
        //            {
        //                throw new HttpCompileException(startStopAgentResult.message);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.ToString());
        //    }
        //}

        //private void getAgentProgress(string agentName, Guid sessionId, string retornoErro)
        //{
        //    var agentProgress = new List<AgentProgress>();
        //    bool processFinished = false;

        //    do //colocar essa consulta em uma thread e fazer ela esperar uns 2 segundos antes de repetir para nao sobreccaregar o servidor com muitas consultas 
        //    {
        //        try
        //        {
        //            using (var httpClient = new HttpClient())
        //            {
        //                var urlOData = string.Format("{0}GetAgentProgressAsJson?agent={1}&sessionId={2}", clientUrl.client.BaseAddress, agentName, sessionId);

        //                httpClient.DefaultRequestHeaders.Accept.Clear();
        //                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //                var response = httpClient.GetAsync(urlOData).Result;
        //                response.EnsureSuccessStatusCode();
        //                var resultado = response.Content.ReadAsStringAsync().Result;
        //                if (!resultado.Equals("[]"))
        //                {
        //                    agentProgress = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<AgentProgress>>(resultado)).Result;

        //                    if (agentProgress.Where(x => x.seq_ == "1").FirstOrDefault().page_status_message.Equals("Idle"))
        //                    {
        //                        processFinished = true;
        //                        //stopAgent(agentName, sessionId);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(retornoErro += ex.ToString());
        //        }
        //    } while (!processFinished);
        //}

        //private string getAgentData(string agentName, Guid sessionId, String retornoErro)
        //{
        //    //client.BaseAddress = new Uri("http://localhost:8004/ContentGrabber/");
        //    var fatura = new FaturaPdfSky();
        //    try
        //    {
        //        var contador = 0;
        //        do
        //        {
        //            using (var httpClient = new HttpClient())
        //            {
        //                var urlOData = string.Format("{0}GetAgentDataAsJson?agent={1}&logLevel=High&sessionId={2}", clientUrl.client.BaseAddress, agentName, sessionId);
        //                //var urlOData = string.Format("{0}GetAgentDataAsJson?agent={1}&logLevel=High&timeOut=70", client.BaseAddress, agentName);

        //                httpClient.DefaultRequestHeaders.Accept.Clear();
        //                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //                var response = httpClient.GetAsync(urlOData).Result;
        //                response.EnsureSuccessStatusCode();
        //                var resultado = response.Content.ReadAsStringAsync().Result;
        //                contador++;
        //                fatura = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<FaturaPdfSky>(resultado)).Result;

        //            }
                    
        //        } while ((fatura.AgentFaturaSky == null) && (contador < 15));

        //        if (String.IsNullOrEmpty(fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY.ToString()))
        //        {
        //            fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY = @"C:\Users\Public\Documents\Content Grabber 2\Agents\SkyAgent_Fatura_UL\Data\Files\5a9580e9-3dcb-4044-bbed-673782edfd77\1b4e9ee9da2f42a7b1feeeb1d970231e\400249447410.html";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return retornoErro += ex.Message;
        //    }

        //    if (!String.IsNullOrEmpty(fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY.ToString()))
        //    {
        //        var str = fatura.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY;
        //        var pdfContent = exec(str, LayoutPath.pathLayout());
        //        return pdfContent;
        //    }
        //    else
        //        return "Consulta nao retornou pdf";
        //}

        /// <summary>
        /// Method that accesses contentgrabber rest api and returns the agent data.
        /// </summary>
        /// <param name="agentName"></param>
        /// <param name="sessionId"></param>
        /// <param name="login"></param>
        /// <returns></returns>
        public string RunAgentFromScrapingToolReturnJson(string agentName, Guid sessionId, Login login)
        {
            var loginSerialized = JsonConvert.SerializeObject(login);

            var agent = new AgentReturn();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var urlOData = string.Format("{0}RunAgentReturnJson?agent={1}&sessionId={2}&pars={3}&logLevel=High&timeOut=120&logtofile=true", clientUrl.client.BaseAddress, agentName, sessionId, loginSerialized);

                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = httpClient.GetAsync(urlOData).Result;
                    response.EnsureSuccessStatusCode();
                    var resultado = response.Content.ReadAsStringAsync().Result;

                    //agent = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<AgentReturn>(resultado)).Result;
                    //if (String.IsNullOrEmpty(agent.data.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY))
                    //{
                    //    throw new ArgumentNullException(resultado);
                    //}
                    
                    agent = Json.Decode<AgentReturn>(resultado);

                    return JsonConvert.SerializeObject(agent.data);
                }
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError){
                        Content = new StringContent("Houve um erro com o retorno do agente."),
                        StatusCode = HttpStatusCode.InternalServerError,
                        ReasonPhrase = "O agente pode estar parado no servidor."
                    });
            }
        }

        private Task delayGetAgentData(int milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer time = new System.Timers.Timer();
            time.Elapsed += (obj, args) =>
            {
                tcs.TrySetResult(true);
            };
            time.Interval = milliseconds;
            time.AutoReset = false;
            time.Start();
            return tcs.Task;
        }

    }
}
