﻿using System;
using System.Net.Http;

namespace ScrapingApi.Servico

{
    public class UrlApiBase
    {
        /// <summary>
        /// Metodo base para a conexao com o servidor do contentGrabber, mas se houverem mais de um servidor para cada cliente, pode-se dar um override no filho e passar o servidor aonde esta o contentgrabber para aquele cliente.
        /// Ex: UrlSky : UrlApiBase e override o client passando a uri de outro servidor.
        /// </summary>
        public HttpClient client = new HttpClient
        {
            BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["ContentGraberApiUrl"].ToString())
        };
    }
}