﻿using Newtonsoft.Json;
using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Dominio.Entities.Clients.Sky;
using ScrapingApi.Dominio.Entities.PdfManagement;
using ScrapingApi.Servico.PdfConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrapingApi.Servico
{
    public class ManipulatePdfUrlToObject
    {
        //public RetornoPdfSky convertPdfUrlToJsonObject(FaturaPdfSky agent)
        public string convertPdfUrlToJsonObject(DataFaturaSky agent)
        {

            var retornoPdf = new RetornoPdfSky();

            if (String.IsNullOrEmpty(agent.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY ))
            {
                retornoPdf.FaturaNum = "1234";
                retornoPdf.DescontoBonus = "desconto bonus";
                retornoPdf.Vencimento = "12/02/2014";
                retornoPdf.ValorTotal = "156,20";
                retornoPdf.ValorItem = "300,01";
                retornoPdf.Periodo = "15 dias";
                retornoPdf.Item = "item teste";
                retornoPdf.DataInicio = "01/03/2017";
                retornoPdf.DataFim = "01/04/2017";
                retornoPdf.CodigoDoClienteNum = "161825";

                return JsonConvert.SerializeObject( retornoPdf);
            }
            else
            {
                var pdfConverter = new PdfConvert();
                var pdfContent = pdfConverter.ConvertPdfWithFileLayout(agent.AgentFaturaSky.FirstOrDefault().PDF_Fatura_SKY, LayoutPath.pathLayout());
                //return retornoPdf = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<RetornoPdfSky>(pdfContent)).Result;
                return pdfContent;
            }
        }

        public DataFaturaSky manipulateAgentFaturaSky(string agentData)
        {
            var agent = new DataFaturaSky();
            agent = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<DataFaturaSky>(agentData)).Result;
            return agent;
        }
    }
}
