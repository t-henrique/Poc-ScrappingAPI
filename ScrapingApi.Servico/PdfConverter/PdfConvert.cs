﻿using ScrapingApi.Dominio.Entities.PdfManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrapingApi.Servico.PdfConverter
{
    public class PdfConvert
    {
        private GetPdfJson pdfJson;

        public string ConvertPdfWithFileLayout(string filePdf, string fileLayout)
        {
            pdfJson = new GetPdfJson(filePdf, fileLayout);
            return pdfJson.GetJson();
        }
    }
}
