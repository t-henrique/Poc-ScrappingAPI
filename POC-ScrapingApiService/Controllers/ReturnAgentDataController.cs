﻿using POC_ScrapingApiService.ClientManagement;
using ScrapingApi.Dominio.Entities.Base;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace POC_ScrapingApiService.Controllers
{
    public class ReturnAgentDataController : ApiController
    {

        public HttpResponseMessage getAgentData([FromUri] ClientEnum client, [FromUri] string method, [FromUri] bool token, [FromUri] string login, [FromUri] string senha)
        {
            var agentDataReturn = String.Empty;
            try
            {
                if (token) //motivo para validar se usuário autorizado
                {
                    agentDataReturn = ClientsRouter.switchClientMethod(client, method, tempFillLogin(login, senha));
                }
                else
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }
            catch (HttpResponseException Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ex.Response.ToString());
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(agentDataReturn, Encoding.UTF8, "application/json");
            return response;
        }

        private Login tempFillLogin(string loginUser, string senhaUser)
        {
            var login = new Login
            {
                login = loginUser,
                password = senhaUser
            };

            return login;

        }
    }
}
