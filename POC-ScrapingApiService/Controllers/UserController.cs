﻿using ScrapingApi.Dominio.Entities.PersistenceObjects;
using ScrapingApi.Dominio.Entities.Rules;
using ScrapingApi.Infra.Persistence;
using System.Collections.Generic;
using System.Web.Http;

namespace POC_ScrapingApiService.Controllers
{
    public class UserController : ApiController
    {
        // GET api/user
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/user/5
        public User Get(string cpf, string password)
        {
            var userRepo = new UserRepository();
            return userRepo.getUser(cpf, password);
        }


        // POST api/user
        public void Post([FromBody] User user)
        {
            var userRepo = new UserRepository();
            var uManipulation = new UserManipulation();
            //user.birthDate = uManipulation.changeDateTimeFromRest(user.birthDate);
            userRepo.saveOrUpdateUser(user);
        }

        // PUT api/user/5
        public void Put([FromBody]User user)
        {
            var userRepo = new UserRepository();
            userRepo.saveOrUpdateUser(user);
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
        }
    }
}
