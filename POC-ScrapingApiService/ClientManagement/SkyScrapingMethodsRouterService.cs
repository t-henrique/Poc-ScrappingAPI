﻿using POC_ScrapingApiService.ClientManagement;
using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Dominio.Entities.Clients.Sky;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POC_ScrapingApiService.ClientManagement
{
    public class SkyScrapingMethodsRouterService : ClientScrapingMethodsRouter<SkyMethods>
    {

        public override string chooseMethod(string method, Login login)
        {
            try
            {
                var clientMethod = (SkyMethods.Methods)Enum.Parse(typeof(SkyMethods.Methods), method);
                switch (clientMethod)
                {
                    case SkyMethods.Methods.fatura:
                        agentDataReturn = clientScapingMethods.getInvoice(login);
                        break;
                    case SkyMethods.Methods.usuario:
                        agentDataReturn = clientScapingMethods.getUserData(login);
                        break;
                    default:
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest));

                }
                return agentDataReturn;
            }
            catch (ArgumentException Ex)
            {
                throw new HttpResponseException(new HttpResponseMessage
                {
                    Content = new StringContent(Ex.Message.ToString()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    ReasonPhrase = "The requested method doesn't exist. Description: " + Ex.Message
                });
            }
        }

    }
}