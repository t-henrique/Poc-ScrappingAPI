﻿using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Servico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POC_ScrapingApiService.ClientManagement
{
    public abstract class ClientMethods<T> where T : new()
    {
        protected Guid sessionId = Guid.NewGuid();
        protected T agentClient = new T();
        protected ConsumesScrapingRestApi consumesApi = new ConsumesScrapingRestApi();
        protected string agentDataReturn = String.Empty;
        
        //to do Uncomment this line and organize to return the data in c# object 
        //public abstract string getInvoice(Login login);

        public abstract string getUserData(Login login);
    }
}