﻿using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Dominio.Entities.Clients.Sky;
using ScrapingApi.Servico;

namespace POC_ScrapingApiService.ClientManagement
{
    public class SkyMethods : ClientMethods<SkyAgents>
    {
        public enum Methods
        {
            usuario,
            fatura
        }

        public string getInvoice(Login login)
        {
           agentDataReturn = consumesApi.RunAgentFromScrapingToolReturnJson(agentClient.AgentReturnFaturaPdf, sessionId, login);
           
           return manipulatesAgentRetun(agentDataReturn);
        }

        public override string getUserData(Login login)
        {
            agentDataReturn = consumesApi.RunAgentFromScrapingToolReturnJson(agentClient.AgentReturnUserData, sessionId, login);
            return agentDataReturn;
        }

        private string manipulatesAgentRetun(string agentDataReturned)
        {
            var mgnt = new ManipulatePdfUrlToObject();
            return mgnt.convertPdfUrlToJsonObject(mgnt.manipulateAgentFaturaSky(agentDataReturn));

        }
    }
}