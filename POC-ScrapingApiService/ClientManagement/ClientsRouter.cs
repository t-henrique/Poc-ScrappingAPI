﻿using ScrapingApi.Dominio.Entities.Base;
using ScrapingApi.Servico;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POC_ScrapingApiService.ClientManagement
{
    public class ClientsRouter
    {
        public static string switchClientMethod(ClientEnum client, string method, Login login)
        {
            try
            {
                switch (client)
                {
                    case ClientEnum.cemig:
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotImplemented));
                    case ClientEnum.net:
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotImplemented));
                    case ClientEnum.sky:
                        {
                            var skyScrapingService = new SkyScrapingMethodsRouterService();
                            return skyScrapingService.chooseMethod(method, login);
                        }

                    default:
                        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("Client not implemented. Please contact your system administrator.")
                        });
                }
            }
            catch (HttpResponseException Ex)
            {
                return Ex.Response.ToString();
            }
            catch (ArgumentException Ex)
            {
                throw new HttpResponseException(new HttpResponseMessage
                    {
                        Content = new StringContent(Ex.Message.ToString()),
                        StatusCode = HttpStatusCode.InternalServerError,
                        ReasonPhrase = "The requested client doesn't exist. Description: " + Ex.Message
                    });
            }
        }
    }
}