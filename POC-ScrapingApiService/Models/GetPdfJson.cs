﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POC_ScrapingApiService.Models
{
    public class GetPdfJson
    {
        GetTextOfLayout GToL;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePDF"></param>
        /// <param name="fileMapFields"></param>
        /// <param name="fileOutput"></param>
        public GetPdfJson(string filePDF, string fileMapFields, string fileOutput = "")
        {
            string[] args = { filePDF, fileMapFields, fileOutput };

            var fm = new FileManipulation(args);
            GToL = new GetTextOfLayout(fm.getArrayFileScrap(), fm.getArrayStyleHtml());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetJson()
        {
            return GToL.GetResult();
        }

        public string[] GetTxt()
        {
            return GToL.GetTxt();
        }
    }
}