﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POC_ScrapingApiService.Models
{
    public class RetornoFatura
    {
        public string DataFatura { get; set; }
        public string ValorFatura { get; set; }
    }
}