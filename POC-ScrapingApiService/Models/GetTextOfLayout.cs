﻿using System;
using System.Collections.Generic;

namespace POC_ScrapingApiService.Models
{
    public class GetTextOfLayout
    {
        private string _resultTxt;
        private string[] _fileTxt;
        private List<char> _layoutTxtBreaked;

        /// <summary>
        /// 
        /// </summary>
        public string GetResult()
        {
            return _resultTxt;
        }

        public string[] GetTxt()
        {
            return _fileTxt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        public GetTextOfLayout(string[] fileTxt, string[] layoutTxt)
        {
            try
            {
                _fileTxt = fileTxt;
                _layoutTxtBreaked = new List<char>();

                BreakLineArray(layoutTxt, ref _layoutTxtBreaked);
                NextExpression();
                //teste();
            }
            catch { return; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="result"></param>
        private void BreakLineArray(string[] arr, ref List<char> result)
        {
            foreach (string str in arr)

                if (IsString(str[0]))
                    foreach (char c in str)
                        result.Add(c);
                else
                    foreach (char c in str)
                        if (!char.IsWhiteSpace(c))
                            result.Add(c);
        }

        /// <summary>
        /// 
        /// </summary>
        private void NextExpression()
        {
            char c;
            for (int i = 0; i < _layoutTxtBreaked.Count; i++)
            {
                c = _layoutTxtBreaked[i];

                //Compare , or :
                if (IsSeparate(c))
                    PrepareDots(c);

                //Compare Numbers
                else if (IsNumber(c))
                {
                    var exp = NumberExpression(ref i);
                    ReadLineNumber(Int32.Parse(exp.ToString()));
                }

                //Compare keys like [] {} <>
                else if (IsKeysControls(c))
                {
                    if (c == '<')
                    {
                        var rc = ReadRowAndCol(c, ref i);
                        ReadLineNumber(rc[0], rc[1]);
                    }
                    else
                        KeysControlMake(c);
                }

                //Compare " or '
                else if (IsString(c))
                    _resultTxt += StringExpression(ref i);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string NumberExpression(ref int index)
        {
            string expNum = string.Empty;

            try
            {
                do
                {
                    expNum += _layoutTxtBreaked[index++];

                } while (IsNumber(_layoutTxtBreaked[index]));
                index--;
            }
            catch
            { index--; }

            return expNum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string StringExpression(ref int index)
        {
            string expNum = string.Empty;

            try
            {
                do
                {
                    expNum += _layoutTxtBreaked[index++];
                } while (_layoutTxtBreaked[index] != '"');
                expNum += _layoutTxtBreaked[index];
            }
            catch
            { index--; }

            return expNum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="idxChar"></param>
        /// <returns></returns>
        private int[] ReadRowAndCol(char c, ref int idxChar)
        {
            char r = '>';
            string[] arrNumRC = new string[2];
            int rowORcol = 0;

            do
            {
                r = _layoutTxtBreaked[++idxChar];

                if (IsNumber(r))
                    arrNumRC[rowORcol] += r;
                else if (r == ',')
                    rowORcol++;

            } while (r != '>');

            return
                new int[] {
                    Int32.Parse(arrNumRC[0]),
                    Int32.Parse(arrNumRC[1])};
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        private void PrepareDots(char c)
        {
            if (c == ',')
            {
                _resultTxt += c;
                _resultTxt += '\n';
            }
            else if (c == ':')
                _resultTxt += " " + c + " ";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        private void KeysControlMake(char c)
        {
            if (c == '{' || c == '[')
            {
                _resultTxt += c;
                _resultTxt += '\n';
            }
            else if (c == '}' || c == ']')
            {
                _resultTxt += '\n';
                _resultTxt += c;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="col"></param>
        private void ReadLineNumber(int n, int col = -1)
        {
            char asp = '\"';
            try
            {
                if (col > -1)
                    _resultTxt += asp + _fileTxt[n].Split(' ')[col] + asp;
                else
                    _resultTxt += asp + _fileTxt[n] + asp;
            }
            catch
            {
                throw new Exception("\nColuna inexistente escolhida no layout do scrap");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private static bool IsNumber(char c)
        {
            return (Char.IsNumber(c));
        }
        private static bool IsSeparate(char c)
        {
            return (c == ',' || c == ':');
        }
        private static bool IsString(char c)
        {
            return (c == '\'' || c == '"');
        }
        private static bool IsKeysControls(char c)
        {
            return (c == '{' || c == '}' ||
                    c == '<' || c == '>' ||
                    c == '[' || c == ']');
        }

    }
}