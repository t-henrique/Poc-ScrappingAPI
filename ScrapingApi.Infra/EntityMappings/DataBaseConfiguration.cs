﻿using NHibernate.Dialect;
using NHibernate.Cfg;
using NHibernate.Driver;
using ScrapingApi.Dominio.Entities.PersistenceObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using NHibernate;

namespace ScrapingApi.Infra.EntityMappings
{
    public class DataBaseConfiguration
    {
        public static ISessionFactory returnSessionFactory()
        {
            var cfg = new Configuration();
            var connectionstring = ConnectionString.ConnectionStringScrappingApi;

            cfg.DataBaseIntegration(x =>
            {
                x.ConnectionString = connectionstring;
                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2012Dialect>();
            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var sessionFactory = cfg.BuildSessionFactory();

            return sessionFactory;
        }
    }
}