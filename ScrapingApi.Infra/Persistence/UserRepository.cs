﻿using NHibernate;
using ScrapingApi.Dominio.Entities.PersistenceObjects;
using ScrapingApi.Dominio.Repository;
using ScrapingApi.Infra.EntityMappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrapingApi.Infra.Persistence
{
    public class UserRepository : IUserRepository
    {
        private ISessionFactory session = DataBaseConfiguration.returnSessionFactory();


        public void saveOrUpdateUser(User user)
        {            
            using (var sessionControl = session.OpenSession())
            {
                {
                    using (var tx = sessionControl.BeginTransaction())
                    {
                        sessionControl.SaveOrUpdate(user);
                        tx.Commit();
                    }
                }
            }
        }


        public User getUser(string cpf, string password)
        {
            var user = new User();
            using (var sessionControl = session.OpenSession())
            {
                {
                    using (var tx = sessionControl.BeginTransaction())
                    {
                        //user = sessionControl.Get<User>(id);
                        user = sessionControl.QueryOver<User>().Where(x => x.cpf == cpf && x.password == password).SingleOrDefault();
                        tx.Commit();
                    }
                }
            }
            return user;
        }
    }
}
