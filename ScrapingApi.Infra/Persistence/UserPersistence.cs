﻿using NHibernate.Cfg;
using ScrapingApi.Dominio.Entities.PersistenceObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NHibernate.Driver;
using NHibernate.Dialect;
using System.Reflection;

namespace ScrapingApi.Infra.Persistence
{
    public class UserPersistence
    {
        public void saveUser(){
            var cfg = new Configuration();

            var connectionstring = ConnectionString.ConnectionStringScrappingApi;

            var user = new User{
                userId  =  1,
                email = "novoemailteste@teste",
                password = "0000",
                authMethod = AuthMethod.fingerPring,
                cpf = "09864589610"
            };

            cfg.DataBaseIntegration( x=> { x.ConnectionString = connectionstring;
                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2012Dialect>();
            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var sefact = cfg.BuildSessionFactory();

            using (var session = sefact.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    //session.Save(user);
                    session.SaveOrUpdate(user);
                    tx.Commit();
                }
            }
        }
    }
}
