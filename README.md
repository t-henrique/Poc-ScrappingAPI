**ScrappingAPI**

## Servico para controle e acesso dos agentes de Scrapping.

As rotas do WebService estao preparadas para responder nesta URL {client}/{method}/{params}

Metodos:

**[GET]: GetClientData()**
* Params:

<code>
LoginSky
{
	login: string,
	password: string,
}
</code>

* Return

<code>
{
	CpfCnpj: string
	Nome: string
	Email: string
}
</code>

**[GET]: GetInvoice()**
* Params:

<code>
LoginSky
{
	login: string,
	password: string,
	
}
</code>
* Return:

<code>
RetornoPdfSky
{
	FaturaNum: string
	CodigoDoClienteNum: string
	Item: string
	DataInicio: string
	DataFim: string
	DescontoBonus: string
	ValorTotal: string
	Vencimento: string
	ValorItem: string
	Periodo: string
}
</code>