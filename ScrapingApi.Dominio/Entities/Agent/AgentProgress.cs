﻿namespace ScrapingApi.Dominio.Entities.Agent
{
    public class AgentProgress
    {
        public string browser_id_ { get; set; }
        public string seq_ { get; set; }
        public string process_row_id_ { get; set; }
        public string controller_id_ { get; set; }
        public string progress_status_message { get; set; }
        public string page_status { get; set; }
        public string page_status_message { get; set; }
    }
}