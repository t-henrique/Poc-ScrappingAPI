﻿
namespace ScrapingApi.Dominio.Entities.Agent
{
    public class AgentReturn
    {
        public AgentInfo status { get; set; }
        public dynamic data { get; set; }
    }
}