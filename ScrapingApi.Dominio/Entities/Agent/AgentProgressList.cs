﻿using System.Collections.Generic;

namespace ScrapingApi.Dominio.Entities.Agent
{
    public class AgentProgressList
    {
        public IEnumerable<AgentProgress> agentProgress { get; set; }
    }
}