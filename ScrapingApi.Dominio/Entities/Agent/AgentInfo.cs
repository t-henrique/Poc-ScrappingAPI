﻿namespace ScrapingApi.Dominio.Entities.Agent
{
    public class AgentInfo
    {
        public string agentName { get; set; }
        public string agentPath { get; set; }
        public string session { get; set; }
        public string lastRuntime { get; set; }
        public string status { get; set; }
        public string pageError { get; set; }
        public string pageLoads { get; set; }
        public string dataCount { get; set; }
        public string lastDataCount { get; set; }
        public string exportRowCount { get; set; }
        public string lastExportRowCount { get; set; }
        public string isScheduleEnabled { get; set; }
        public string nextRunTime { get; set; }

    }
}