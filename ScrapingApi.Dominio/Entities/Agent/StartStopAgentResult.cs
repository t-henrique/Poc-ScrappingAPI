﻿
namespace ScrapingApi.Dominio.Entities.Agent
{
    public class StartStopAgentResult
    {
        public bool success { get; set; }
        public string message { get; set; }
    }
}