﻿using System.Collections.Generic;

namespace ScrapingApi.Dominio.Entities.Clients.Sky
{
    public class DataFaturaSky
    {
        public List<AgentFaturaSky> AgentFaturaSky { get; set; }
    }
}