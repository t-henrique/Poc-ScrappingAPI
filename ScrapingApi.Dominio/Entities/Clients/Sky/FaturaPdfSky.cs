﻿using ScrapingApi.Dominio.Entities.Agent;

namespace ScrapingApi.Dominio.Entities.Clients.Sky
{
    public class FaturaPdfSky : AgentFaturaPdf
    {
        public DataFaturaSky data { get; set; }
    }
}