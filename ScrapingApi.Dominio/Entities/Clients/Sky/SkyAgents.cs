﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ScrapingApi.Dominio.Entities.Clients.Sky
{
    public class SkyAgents 
    {
        public string AgentReturnUserData = System.Configuration.ConfigurationManager.AppSettings["AgentUserData"].ToString();
        public string AgentReturnFaturaPdf = System.Configuration.ConfigurationManager.AppSettings["AgentFaturaSky"].ToString();
    }
}