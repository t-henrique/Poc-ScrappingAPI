﻿namespace ScrapingApi.Dominio.Entities.Base
{
    public class UserData
    {
        public virtual string nome { get; set; }
        public virtual string cpfCnpj { get; set; }
        public virtual string email { get; set; }
    }
}