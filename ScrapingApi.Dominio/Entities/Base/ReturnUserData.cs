﻿using ScrapingApi.Dominio.Entities.Agent;
using System.Collections.Generic;

namespace ScrapingApi.Dominio.Entities.Base
{
    public class ReturnUserData
    {
        public AgentInfo status { get; set; }
        public virtual List<UserData> userData { get; set; }
    }
}