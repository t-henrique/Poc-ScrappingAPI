﻿
namespace ScrapingApi.Dominio.Entities.Base
{
    public class RetornoPdfJson
    {
        public virtual string FaturaNum { get; set; }
        public virtual string CodigoDoClienteNum { get; set; }
        public virtual string Item { get; set; }
        public virtual string DataInicio { get; set; }
        public virtual string DataFim { get; set; }
        public virtual string DescontoBonus { get; set; }
        public virtual string ValorTotal { get; set; }
        public virtual string Vencimento { get; set; }
        public virtual string ValorItem { get; set; }
        public virtual string Periodo { get; set; }
    }
}