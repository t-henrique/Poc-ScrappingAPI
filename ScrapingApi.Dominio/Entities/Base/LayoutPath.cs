﻿namespace ScrapingApi.Dominio.Entities.Base
{
    public static class LayoutPath
    {
        private static string fileName = "LayoutExemploParaJson.txt";
        static string path;
        /// <summary>
        /// Caminho padrao para o layout 
        /// C:\projeto\POC-ScrapingApiService\POC-ScrapingApiService\POC-ScrapingApiService\LayoutExemploParaJson.txt
        /// </summary>
        /// <returns></returns>
        public static string pathLayout()
        {
            return path = System.Configuration.ConfigurationManager.AppSettings["LayoutExemploParaJsonDefault"].ToString();
            //return path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
        }

    }
}