﻿using System;
using System.IO;
using System.Text;

namespace ScrapingApi.Dominio.Entities.PdfManagement
{
    public class FileManipulation
    {
        private static StreamReader _srFile;

        private static Encoding _encoding = Encoding.UTF8;
        private static string[] _arraySimbols = { "<b>", "</b>", "<br>" };
        private static string[] _arraySimbolsInLine = { " &nbsp;", "&nbsp; ", "&nbsp;" };

        private string[] _arrayFileScrap;
        private string[] _arrayStyleHtml;
        private string _outputfile;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string[] getArrayFileScrap()
        {
            return _arrayFileScrap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string[] getArrayStyleHtml()
        {
            return _arrayStyleHtml;
        }

        public void WriteFile(string strResult)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(_outputfile))
                {
                    foreach (char s in strResult) //quebrar a linha
                    {
                        if (s == '\n')
                            sw.WriteLine();
                        else
                            sw.Write(s);
                    }
                }
            }
            catch { return; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param> 
        public FileManipulation(string[] args)
        {
            try
            {
                _arrayFileScrap = LoadFile(args[0]);
                _arrayStyleHtml = LoadFile(args[1]);

                if (args.Length > 2)
                    _outputfile = args[2];
            }
            catch
            {
                if (args.Length < 2)
                    Console.WriteLine("Favor incluir o diretório e dos arquivos");

                else if (args.Length > 3)
                    Console.WriteLine("Exceço de parametros");

                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string[] LoadFile(string fileName)
        {
            string[] lines = new string[0];
            try
            {
                _srFile = new StreamReader(fileName);
                string txt = _srFile.ReadToEnd();

                if (txt.Length < 0)
                    throw new Exception("O arquivo " + fileName + " está vazio.");

                lines = txt.Split('\r');
            }
            catch (Exception e) { };

            return GetLineNoCharsetSimbol(lines);
        }

        /// <summary>
        /// Remover os simbolos do txt
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        private string[] GetLineNoCharsetSimbol(string[] rows)
        {
            for (int i = 0; i < rows.Length; i++)
            {
                foreach (string charR in _arraySimbolsInLine)
                    rows[i] = rows[i].Replace(charR, " ").TrimStart().TrimEnd();// +'\n';

                foreach (string charR in _arraySimbols)
                    rows[i] = rows[i].Replace(charR, string.Empty);
            }

            return rows;
        }

    }
}