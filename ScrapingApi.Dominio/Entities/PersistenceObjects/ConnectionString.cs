﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ScrapingApi.Dominio.Entities.PersistenceObjects
{
    public static class ConnectionString
    {
        public static string ConnectionStringScrappingApi
        {
            get
            {
                var configuracao = ConfigurationManager.ConnectionStrings["ScrappingApiDatabase"].ConnectionString;

                if (!string.IsNullOrWhiteSpace(configuracao))
                    return configuracao;

                throw new ConfigurationErrorsException(
                    "Nenhum valor para a connection string \"Hominum\". " +
                    "Favor configurar a string de conexão no arquivo de configuração da aplicação.");
            }
        }
    }
}
