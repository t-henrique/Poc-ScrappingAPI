﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScrapingApi.Dominio.Entities.PersistenceObjects
{
    public class User
    {
        public virtual int userId { get; set; }
        public virtual string name { get; set; }
        public virtual DateTime birthDate { get; set; }
        public virtual string email { get; set; }
        public virtual string password  { get; set; }
        public virtual string cpf { get; set; }
        public virtual AuthMethod authMethod { get; set; }
        public virtual string phone { get; set; }
    }

    public enum AuthMethod{
        fingerPring = 1,
        voice = 2, 
        password = 3
    }
}
