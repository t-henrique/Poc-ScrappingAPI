﻿using ScrapingApi.Dominio.Entities.PersistenceObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ScrapingApi.Dominio.Entities.Rules
{
    public class UserManipulation : User
    {
        public DateTime changeDateTimeFromRest(DateTime birthday)
        {
            this.birthDate = DateTime.ParseExact(birthday.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            return birthDate;
        }
    }
}
