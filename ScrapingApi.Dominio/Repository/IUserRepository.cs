﻿using ScrapingApi.Dominio.Entities.PersistenceObjects;

namespace ScrapingApi.Dominio.Repository
{
    public interface IUserRepository
    {
        void saveOrUpdateUser(User user);
        User getUser(string cpf, string password);
    }
}
